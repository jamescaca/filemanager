﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using FileManagerAPI.Models;

namespace FileManagerAPI.Controllers
{
    public class filesController : ApiController
    {
        private MergedFilesEntities db = new MergedFilesEntities();

        // GET: api/files
        public IQueryable<file> Getfiles()
        {
            return db.files;
        }

        // GET: api/files/5
        [ResponseType(typeof(file))]
        public IHttpActionResult Getfile(int id)
        {
            file file = db.files.Find(id);
            if (file == null)
            {
                return NotFound();
            }

            return Ok(file);
        }

        // PUT: api/files/5
        [ResponseType(typeof(void))]
        public IHttpActionResult Putfile(int id, file file)
        {
            //if (!ModelState.IsValid)
            //{
            //    return BadRequest(ModelState);
            //}

            if (id != file.id)
            {
                return BadRequest();
            }

            db.Entry(file).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!fileExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/files
        [ResponseType(typeof(file))]
        public IHttpActionResult Postfile(file file)
        {
            //if (!ModelState.IsValid)
            //{
            //    return BadRequest(ModelState);
            //}

            db.files.Add(file);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = file.id }, file);
        }

        // DELETE: api/files/5
        [ResponseType(typeof(file))]
        public IHttpActionResult Deletefile(int id)
        {
            file file = db.files.Find(id);
            if (file == null)
            {
                return NotFound();
            }

            db.files.Remove(file);
            db.SaveChanges();

            return Ok(file);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool fileExists(int id)
        {
            return db.files.Count(e => e.id == id) > 0;
        }
    }
}