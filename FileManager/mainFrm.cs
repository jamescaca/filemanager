﻿using Microsoft.VisualBasic.FileIO;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;

namespace FileManager
{
    public partial class mainFrm : Form
    {
        //Puting the upload funciton into a background thread
        string path = String.Empty;
        //var fileContent = String.Empty;
        public mainFrm()
        {
            InitializeComponent();
            initialize();
        }

        private void initialize()
        {
            DataTable csv;
            btnUpload.Click += (s, e) =>
            {
                using (FolderBrowserDialog dialog = new FolderBrowserDialog())
                {
                    if (dialog.ShowDialog() == DialogResult.OK && !string.IsNullOrWhiteSpace(dialog.SelectedPath))
                    {
                        string[] files = Directory.GetFiles(dialog.SelectedPath, "*.csv", System.IO.SearchOption.TopDirectoryOnly);
                        MessageBox.Show("Upload Starts!");
                        foreach (var file in files)
                        {
                            csv = GetDataTabletFromCSVFile(file);
                            InsertDataIntoSQLServerUsingSQLBulkCopy(csv, file);
                        }
                        MessageBox.Show("Upload Ends!");
                    }
                };
            };

            btnDrop.Click += (s, e) =>
            {
                DialogResult dialog = MessageBox.Show("Do you want to drop the files table?","Drop Talbe!",MessageBoxButtons.OKCancel);
                if (dialog == DialogResult.OK)
                {
                    dropTable("files");
                    MessageBox.Show("Table Dropped!");
                }
                else { MessageBox.Show("Table Remains Here!"); }
            };
        }

        #region detail implementation
        private void dropTable(string tablename)
        {
            var connection = System.Configuration.ConfigurationManager.ConnectionStrings["Files"].ConnectionString;
            var query = String.Format(@"
            drop table {0} 
            ", tablename);
            try
            {
                using (SqlConnection conn = new SqlConnection(connection))
                {
                    using (SqlCommand cmd = new SqlCommand(query, conn))
                    {
                        conn.Open();
                        cmd.ExecuteNonQuery();
                        conn.Close();
                    }
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }


        private void createDB(string[] fields)
        {
            var connection = System.Configuration.ConfigurationManager.ConnectionStrings["Files"].ConnectionString;
            var query = @"IF OBJECT_ID (N'dbo.files', N'U') IS NULL
            create table files (
            id int primary key identity(1,1),
            SpecialPurposeColumns XML COLUMN_SET FOR ALL_SPARSE_COLUMNS,
            filename varchar(255) SPARSE null)
            ";

            string exists = null;

            //check existence
            try
            {
                using (SqlConnection conn = new SqlConnection(connection))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        conn.Open();
                        cmd.Connection = conn;
                        cmd.CommandText = "SELECT OBJECT_ID (N'dbo.files', N'U')";
                        exists = cmd.ExecuteScalar().ToString();
                        conn.Close();
                    }
                }
            }
            catch (Exception e) { MessageBox.Show(e.Message); }

            if (exists == null || exists == string.Empty)
            {
                //create talbe
                try
                {
                    using (SqlConnection conn = new SqlConnection(connection))
                    {
                        using (SqlCommand cmd = new SqlCommand(query, conn))
                        {
                            conn.Open();
                            cmd.ExecuteNonQuery();
                            conn.Close();
                        }
                    }
                }
                catch (Exception e) { MessageBox.Show(e.Message); }

                //adding columns
                try
                {
                    using (SqlConnection conn = new SqlConnection(connection))
                    {
                        using (SqlCommand cmd = new SqlCommand())
                        {
                            conn.Open();
                            foreach (string field in fields)
                            {
                                cmd.Connection = conn;
                                cmd.CommandText = "ALTER TABLE " + "files" + " ADD [" + field + "] varchar(255) SPARSE";
                                cmd.ExecuteNonQuery();
                            }
                            conn.Close();
                        }
                    }
                }
                catch (Exception e) { MessageBox.Show(e.Message); }
            }
        }

        private DataTable GetDataTabletFromCSVFile(string csv_file_path)
        {
            DataTable csvData = new DataTable();
            try
            {
                using (TextFieldParser csvReader = new TextFieldParser(csv_file_path))
                {
                    csvReader.SetDelimiters(new string[] { "," });
                    csvReader.HasFieldsEnclosedInQuotes = true;
                    string[] colFields = csvReader.ReadFields();
                    foreach (string column in colFields)
                    {
                        DataColumn datecolumn = new DataColumn(column);
                        datecolumn.AllowDBNull = true;
                        csvData.Columns.Add(datecolumn);
                    }
                    //Create DB
                    createDB(colFields);
                    //Loadding CSV
                    while (!csvReader.EndOfData)
                    {
                        string[] fieldData = csvReader.ReadFields();
                        //Making empty value as null
                        for (int i = 0; i < fieldData.Length; i++)
                        {
                            if (fieldData[i] == "")
                            {
                                fieldData[i] = null;
                            }
                        }
                        csvData.Rows.Add(fieldData);
                    }

                    DataColumn col = new DataColumn("filename");
                    col.AllowDBNull = true;
                    csvData.Columns.Add(col);
                    foreach (DataRow row in csvData.Rows)
                    {
                        row["filename"] = Path.GetFileName(csv_file_path);
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return csvData;
        }

        private void InsertDataIntoSQLServerUsingSQLBulkCopy(DataTable csvFileData, string filename)
        {
            var connection = System.Configuration.ConfigurationManager.ConnectionStrings["Files"].ConnectionString;
            try
            {
                using (SqlConnection dbConnection = new SqlConnection(connection))
                {
                    dbConnection.Open();
                    using (SqlBulkCopy s = new SqlBulkCopy(dbConnection))
                    {
                        s.DestinationTableName = "files";
                        foreach (var column in csvFileData.Columns)
                            s.ColumnMappings.Add(column.ToString(), column.ToString());

                        s.WriteToServer(csvFileData);
                        //The issue is due to column set in the wide table.  
                        //But we have to use the wide table since there are more than 1024 columns
                        //Table structure is crated successfully
                    }
                }
            }
            catch (Exception e) { MessageBox.Show(e.Message); }
        }
        #endregion
    }
}