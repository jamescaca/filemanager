1.Project Description
*.FileManager -> used to upload csv files to DB
*.FileManagerAPI -> CURD API based on Web API2 framework in ASP .NET MVC
*.File Viewer -> API .NET MVC project consuming Web APIs and present a user view.

2.How to start and verify the project

*.Run FileManager -> just to import csv files and get table structure ready with C#.  To be notified, since we have 1900+ columns, we are using Wide Table design in SQL Server 2017.  Bulk Copy (BCP) is not supported yet.  Regular table only contains 1024 columns maxium.

*.Right Click Solution icon and set both Web Api and Viewer projects started.

*.In the viewer project:
~/File/Index -> list all items

Click Create Button -> jump to AddOrEdit page -> Post new object to DB by clicking Submit.

Click Edit -> load specific item to AddOrEdit page -> Put/Update object in DB by clicking Submit.
Click Delete -> drop the specific row in DB and redirect to home page.


3.Project Dependencies
*.There 3 project are totally independent and easy to maintain.

4.Library Dependencies
For FileManager:
*.Microsoft.VisualBasic
*.System.Configuration

For viewer project:
*.Microsoft.AspNet.WebApi.Client
*.alertifyjs -> easier to manage the popup messages in front-end

For Both Api and viewer project
*.Entity Framework


