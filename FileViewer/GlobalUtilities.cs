﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;

namespace FileViewer
{
    public static class GlobalUtilities
    {
        public static HttpClient WebApiClient = new HttpClient();

        static GlobalUtilities() {
            WebApiClient.BaseAddress = new Uri("http://localhost:52135/api/");
            WebApiClient.DefaultRequestHeaders.Clear();
            WebApiClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }
    }
}