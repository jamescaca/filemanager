﻿using System.Collections.Generic;
using System.Web.Mvc;
using FileViewer.Models;
using System.Net.Http;

namespace FileViewer.Controllers
{
    public class FileController : Controller
    {
        // GET: File
        public ActionResult Index()
        {
            IEnumerable<MVCFileModel> fileList;
            HttpResponseMessage response = GlobalUtilities.WebApiClient.GetAsync("files").Result;
            fileList = response.Content.ReadAsAsync<IEnumerable<MVCFileModel>>().Result;
            return View(fileList);
        }

        public ActionResult AddOrEdit(int id = 0) {
            if (id == 0)
            {
                return View(new MVCFileModel());
            }
            else
            {
                HttpResponseMessage res = GlobalUtilities.WebApiClient.GetAsync("Files/"+id.ToString()).Result;
                return View(res.Content.ReadAsAsync<MVCFileModel>().Result);
            }
        }

        [HttpPost]
        public ActionResult AddOrEdit(MVCFileModel file) {
            if (file.id == 0)
            {
                HttpResponseMessage res = GlobalUtilities.WebApiClient.PostAsJsonAsync("Files", file).Result;
                TempData["SuccessMsg"] = "Saved";
            }
            else {
                HttpResponseMessage res = GlobalUtilities.WebApiClient.PutAsJsonAsync("Files/" + file.id.ToString(),file).Result;
                TempData["SuccessMsg"] = "Updated";
            }
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id) {
            //Deletefile
            HttpResponseMessage res = GlobalUtilities.WebApiClient.DeleteAsync("Files/" + id.ToString()).Result;
            TempData["SuccessMsg"] = "Deleted";
            return RedirectToAction("Index");
        }

    }
}